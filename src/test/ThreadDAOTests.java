import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTests {

    private JdbcTemplate mockJdbc;

    @BeforeEach
    void PreTestBehaviour() {
        mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
    }

    @Test
    @DisplayName("Tree sort full statement coverage test #1")
    void TreeSortTest1() {
        ThreadDAO.treeSort(0,null, null, false);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class));
    }

    @Test
    @DisplayName("Tree sort full statement coverage test #2")
    void TreeSortTest2() {
        ThreadDAO.treeSort(0,null, null, true);
        verify(mockJdbc).query(Mockito.eq("SELECT * FROM \"posts\" WHERE thread = ?  ORDER BY branch DESC ;"), Mockito.any(PostDAO.PostMapper.class), Mockito.any(Object.class));
    }

}
